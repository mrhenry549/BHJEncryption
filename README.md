# BHJEncryption
Projeto final de curso sobre encriptação de mensagens usando cifras utilizadas no Escutismo

Cifras implementadas na aplicação:

- Cifra de César;
- Cifra Romano-Árabe;
- Código Morse;
- Código +3.
